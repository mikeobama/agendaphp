<?php
session_start ();

include "comunes.php";
include 'consultas.php';

cabecera ( "Borrar" );

// borrar por id
if (isset ( $_GET ['id'] )) {
	$aux = $_GET ['id'];
	$resultado = listado_id ( $aux );
	
	foreach ( $resultado as $emp ) {
		$id = $emp ['id'];
		$nombre = $emp ['nombre'];
		$apellidos = $emp ['apellidos'];
		$telefono = $emp ['telefono'];
		$correo = $emp ['correo'];
	}
	
	$form = <<<EOFORM
	<div class="container">
<form action="pagina_borrar.php" class="form-signin" method="get">
		<label for=nombre>Nombre:</label> <input type="text" name="nombre"
			id="nombre" class="form-control" value="$nombre" disabled/></br> <label
			for=apellidos>Apellidos:</label> <input type="text" name="apellidos"
			id="apellidos" class="form-control" value="$apellidos" disabled/></br> <label
			for=telefono>Telefono:</label> <input type="text" name="telefono"
			id="telefono" class="form-control" value="$telefono" disabled /></br> <label
			for=email>Email:</label> <input type="email" name="email" id="email"
			value="$correo" class="form-control" disabled/> </br>
			<input type="hidden" name="id" value="$id" />

		<p class="der">
			<input type="submit" name="mod" value="Borrar" class="btn btn-primary boton"/> 
			<input type="submit" value="volver" name="vol" class="btn btn-primary boton"/>
		</p>

</form>
	</div>
EOFORM;
	echo $form;
	
	//borrar todo
} else {
	$form2=<<<EOFORM
		
		<div class="container">
		<form class="form-signin" action="pagina_borrar.php" method="get">
		<h4>¿De verdad quieres eliminar todos los registros?</h4>
		<input type="submit" name="borrar" class="btn btn-primary boton" value="Borrar" /> 
		<input type="submit" value="Cancelar" class="btn btn-primary boton" name="canc" />
		</form>	</div>
EOFORM;
	echo $form2;
}

// comprobar si está activado en sesión
if ($_SESSION ['__valido']) {
	
	if (isset ( $_GET ['mod'] )) {
		
		$nom = $_GET ['nombre'];
		$ap = $_GET ['apellidos'];
		$tel = $_GET ['telefono'];
		$em = $_GET ['email'];
		
		$sql = "delete from agenda where id=$aux";
		
		borrarContacto ( $sql );
		
		header ( 'Location: pagina_listar.php' );
	} elseif (isset ( $_GET['canc']) or isset ($_GET['vol'])  ) {
		header ( 'Location: pagina_listar.php' );
	}elseif (isset($_GET['borrar'])){
		borrarTodo();
		header ( 'Location: pagina_listar.php' );
	}
} else {
	
	header ( 'location: index.php' );
}
?>


<?php

?>	

