<?php
session_start();
include "comunes.php";
include 'consultas.php';

cabecera ( "Editar" );

$aux = $_GET ['id'];
$resultado = listado_id ( $aux );
foreach ( $resultado as $emp ) {
	$id=$emp['id'];
	$nombre = $emp ['nombre'];
	$apellidos = $emp ['apellidos'];
	$telefono = $emp ['telefono'];
	$correo = $emp ['correo'];
}

// comprobar si está activado en sesión
if ($_SESSION['__valido']) {

	if (isset ( $_GET ['mod'] )) {
	
		$nom=strip_tags(htmlspecialchars(trim($_GET['nombre'])));
		$ap=strip_tags(htmlspecialchars(trim($_GET['apellidos'])));
		$tel=strip_tags(htmlspecialchars(trim($_GET['telefono'])));
		$em=$_GET['email'];
		if(!filter_var($em,FILTER_VALIDATE_EMAIL)){
			echo 'Error en email';
		}
		
		$sql = "update agenda set nombre=\"$nom\", apellidos=\"$ap\",
				telefono=\"$tel\", correo=\"$em\" where id=$aux";
		
		modificar($sql);
		
		header ( 'Location: pagina_listar.php' );

}	elseif (isset ( $_GET ['reset'] )) {
		header ( 'Location: pagina_listar.php' );
	}
}else {
	header ( 'location: index.php' );
	}
?>
<div class="container">
<form action="pagina_editar.php" class="form-signin" method="get">
		<label for=nombre>Nombre:</label> <input type="text" name="nombre"
			id="nombre" class="form-control" value="<?php echo $nombre; ?>" /></br> <label
			for=apellidos>Apellidos:</label> <input type="text" name="apellidos"
			id="apellidos" class="form-control" value="<?php echo $apellidos; ?>" /></br> <label
			for=telefono>Telefono:</label> <input type="text" name="telefono"
			id="telefono" class="form-control" value="<?php echo $telefono; ?>" /></br> <label
			for=email>Email:</label> <input type="email" name="email" id="email"
			class="form-control" value="<?php echo $correo; ?>" /> </br>
			<input type="hidden" name="id" value="<?php echo $id; ?>" />

		<p class="der">
			<input type="submit" name="mod" value="Modificar" class="btn btn-primary boton"/> 
			<input type="submit" class="btn btn-primary boton" value="Volver" name="reset" />
		</p>
</form>
</div>
<?php

?>	

