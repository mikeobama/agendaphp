<?php 
session_start ();


require_once "comunes.php";
cabecera ( "Inicio" );


if (isset($_GET['exit'])){
	
	unset($_SESSION['__valido']);
	session_destroy();
	header('Location:index.php');
	
}

?>

<div class="container">
<form method="get" action="logout.php" class="form-signin">
	<h3 style="text-align: center">¿Estás seguro que quieres salir?</h3>
	<br> <input type="submit" class="btn-lg btn-primary btn-block" value="Salir" name="exit">
</form>
</div>