<?php


function listado(){
	try{
		$conn=new PDO('sqlite:agenda.db');
		//select
		$listado="Select * from agenda order by apellidos";
		$resultado=$conn->query($listado);
		
		return $resultado;
	
	}catch (PDOException $e){
		echo $e->getMessage();
	}
}

function usuarios(){
	try{
		$conn=new PDO('sqlite:users.db');
		//select
		$listado="Select nombre,password from users";
		$resultado=$conn->query($listado);

		return $resultado;

	}catch (PDOException $e){
		echo $e->getMessage();
	}
}

function listado_id($id){
	
	try{
		$conn=new PDO('sqlite:agenda.db');
		//select
		$listado="Select * from agenda where id=$id";
		$resultado=$conn->query($listado);
	
		return $resultado;
	
	}catch (PDOException $e){
		echo $e->getMessage();
	}
	
}
function busqueda($aux){

	try{
		$conn=new PDO('sqlite:agenda.db');
		//select
		$listado="Select * from agenda where nombre like '%$aux%' or apellidos like '%$aux%' 
				or telefono like '%$aux%' or correo like '%$aux%'";
		$resultado=$conn->query($listado);

		return $resultado;

	}catch (PDOException $e){
		echo $e->getMessage();
	}

}

function modificar($update){
	
	try{
		$conn=new PDO('sqlite:agenda.db');

		
		$conn->exec($update);
		//$sentencia->bindParam(':nombre', $_GET['nombre']);
		/*$sentencia->bindParam(':apellidos', $apellidos);
		$sentencia->bindParam(':telefono', $telefono);
		$sentencia->bindParam(':email', $email);*/
		
		//$sentencia->execute();
		/*
		 * 		$conn->prepare( $update );
				$conn->execute();
		 */
	
		
	
	}catch (PDOException $e){
		echo $e->getMessage();
	}
	
}
function borrarTodo(){
	try{
		$conn = new PDO('sqlite:agenda.db');
	
		$conn->exec('drop table agenda');
	
	}
	catch(PDOException $e){
		echo $e->getMessage();
	}
}

function borrarContacto($delete){
	try{
		$conn = new PDO('sqlite:agenda.db');
	
		$conn->exec($delete);
		
	} 
	catch(PDOException $e){
		echo $e->getMessage();
	} 
}
?>