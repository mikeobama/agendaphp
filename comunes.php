<?php
function cabecera($texto)
{
	print "<!DOCTYPE html>
	<head>
	<meta http-equiv='content-type' content='text/html; charset=utf-8' />
	
	<!-- Latest compiled and minified CSS -->
<link rel='stylesheet' href='//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css'>
<link type='text/css' rel='stylesheet' href='css/styles.css'>
	<style type='text/css' title='currentStyle'>
@import 'datatables/media/css/demo_table.css';

@import 'datatables/media/css/demo_page.css';
</style>
	<title>$texto</title>
	
	</head>

	<body>
	
	
	<nav class='navbar navbar-inverse' role='navigation'>
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class='navbar-header'>
			<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-ex6-collapse'>
					<span class='sr-only'>Desplegar navegación</span>
					<span class='icon-bar'></span>
							<span class='icon-bar'></span>
									<span class='icon-bar'></span>
      </button>
	      <a class='navbar-brand' href='index.php'>Agenda</a>
	      		</div>
	
	      		<!-- Collect the nav links, forms, and other content for toggling -->
	      		<div class='collapse navbar-collapse navbar-ex6-collapse'>
	      				<ul class='nav navbar-nav'>
	      						<li><a href='pagina_add.php'>Añadir</a></li>
	      						<li><a href='pagina_listar.php'>Contactos</a></li>
	      						<li><a href='pagina_borrar.php'>Borrar</a></li>
	      						<li><a href='pagina_buscar.php'>Buscar</a></li>
	      						</ul>
	      						<p class='navbar-text pull-right'><a href='logout.php'>$_SESSION[__valido]</a></p>
	      						</div><!-- /.navbar-collapse -->
	      						</nav></div>";
}


?>