<?php

function form(){
	$formulario=<<<EOFORM
	<div class="container">
		<form action="index.php" method="post" class="form-signin">
			<h2 class="form-signin-heading">Inicia Sesión</h2>
			<input type="text" class="form-control" name="nombre" placeholder="Nombre" required autofocus/></br>
			<input type="password" name="pass" class="form-control" placeholder="Password" required/></br>
			<p class="der">
				<input type="submit" name="mod" value="Añadir" class="btn btn-lg btn-primary btn-block"/> 
			</p>
		</form>
	</div>
EOFORM;
	echo $formulario;
}


function generateHash($password) {
	if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
		$salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
		return crypt($password, $salt);
	}
}
function verificar($password, $hashedPassword) {
	return crypt($password, $hashedPassword) == $hashedPassword;
}

?>