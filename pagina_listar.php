<?php

session_start ();

require_once "comunes.php";
include 'consultas.php';

cabecera("Listado");

?>

<?php 

		$resultado=listado();		
		echo '<table id="example" class="display">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellidos</th>
						<th>Telefono</th>
						<th>Email</th>
						<th>Modificar</th>
						<th>Borrar</th>
					</tr>
				</thead>';
		foreach ($resultado as $emp){
			//echo '<div class="lista"><ul style="width:240px;list-style:none;border:1px red solid">';
			//echo "<li>Nombre: <strong>", $emp['nombre'],'</strong></li>';
			echo "<tr  class='odd gradeX'><td>".$emp['nombre']."</td>";
			//echo "<li>Apellidos: <strong>", $emp['apellidos'],'</strong></li>';
			echo "<td>".$emp['apellidos']."</td>";
			//echo "<li>Telefono: <strong>", $emp['telefono'],'</strong></li>';
			echo "<td>".$emp['telefono']."</td>";
			//echo "<li>Email: <strong>", $emp['correo'],'</strong></li>';
			echo "<td>".$emp['correo']."</td>";
			echo "<td><a class='enlaces' href='pagina_editar.php?id=$emp[id]'>
				<img alt='editar' src='img/editar.png' width=24px></a></td>";
			echo "<td><a class='enlaces' alt='Borrar' href='pagina_borrar.php?id=$emp[id]'>
				<img alt='borrar' src='img/eliminar.png' width=24px></a></td></tr>";
			//echo '</ul></div>';
		}

?>
<script type="text/javascript" src="datatables/media/js/jquery.js"></script>
<script type="text/javascript"
	src="datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$("#example").dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": true,
		"oLanguage": {
			"sProcessing": "Procesando...",
		    "sLengthMenu": "Mostrar _MENU_ registros",
		    "sZeroRecords": "No se encontraron resultados",
		    "sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "No existen registros",
		    "sSearch": "Buscar:",
			"oPaginate": {
				"sFirst":    "Primero",
				"sPrevious": "Anterior",
				"sNext":     "Siguiente",
				"sLast":     "Último"
			}
		}
		});
	});
</script>
</div>
</body>
</html>