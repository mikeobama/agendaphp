<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();

require_once "comunes.php";

cabecera("Añadir");



$formulario=<<<EOFORM
<div class="container">
<form action="pagina_add.php" method="get" class="form-signin">
  
	<label for=nombre>Nombre:</label>
    <input type="text" class="form-control" name="nombre" id="nombre" autofocus/></br>
	
	<label for=apellidos>Apellidos:</label>
    <input type="text" class="form-control" name="apellidos" id="apellidos" /></br>	
		
	<label for=telefono>Telefono:</label>
    <input type="text" class="form-control" name="telefono" id="telefono" /></br>

	<label for=email>Email:</label>
    <input type="email" class="form-control" name="email" id="email" />	</br>

    <p class="der">
    <input type="submit" class="btn btn-primary boton" value="Añadir" />
    <input type="reset" class="btn btn-primary boton" value="Borrar" name="Reset" /></p>

</form>
</div>
EOFORM;

echo $formulario;


//pie();
?>

<?php 
// comprobar si está activado en sesión

if  ($_SESSION ['__valido']) {
try {
	if(isset($_GET['email'])){
		$nombre=$_GET['nombre'];
		$apellidos=$_GET['apellidos'];
		$telefono=$_GET['telefono'];
		$email=$_GET['email'];
		//crear base de datos
		$conn=new PDO('sqlite:agenda.db');
		//$conn=new PDO('sqlite::memory:');  en memoria
		//insertar datos
		//1.preparar sentencia de insercion
		$insertar="insert into agenda(nombre,apellidos,telefono,correo)
				values(:nombre,:apellidos,:telefono, :email)";
		$sentencia=$conn->prepare($insertar);
		$sentencia->bindParam(':nombre', $nombre);
		$sentencia->bindParam(':apellidos', $apellidos);
		$sentencia->bindParam(':telefono', $telefono);
		$sentencia->bindParam(':email', $email);

		$sentencia->execute();
		header ( 'Location: pagina_listar.php' );
		//select
	}
}catch (PDOException $e){
	echo $e->getMessage();
}
$conn=null;

}else {

	//header ( 'location: index.php' );
}




?>